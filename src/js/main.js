window.onload = function(){

    /**
     * Init Images lazy loading
     * https://github.com/verlok/lazyload
     * DO NOT set src attribute to img tag.
     * SET data-original="path_to_img"
     * SET CSS class .lazyload to all img tags
     * Example: <img class="lazyload" data-original="img/img.png" alt="alt text" title="title text">
     *
     */
    var AMGLazyLoad = new LazyLoad({
        elements_selector: ".lazyload", // img
        data_srcset: "original" // original-set
    });
};
$(window).load(function () {

 $(".spb").click();

});

$("select").select2({
  minimumResultsForSearch: Infinity
});

$('.route-form').submit(function(e){
    e.preventDefault();
    $('.partners').hide();
    $('.results').show();
});

!function(e){function a(e,a){var s=new RegExp("(^|\\s)"+a+"(\\s|$)","g");s.test(e.className)||(e.className=(e.className+" "+a).replace(/\s+/g," ").replace(/(^ | $)/g,""))}function s(e,a){var s=new RegExp("(^|\\s)"+a+"(\\s|$)","g");e.className=e.className.replace(s,"$1").replace(/\s+/g," ").replace(/(^ | $)/g,"")}function t(e,a){var s=new RegExp("(^|\\s)"+a+"(\\s|$)","g");return s.test(e.className)?!0:!1}var r=document.querySelectorAll.bind(document);e.pureTabs={toggle:function(e){var t=this.activeClassName,c=r("[data-puretabs]")[0],l=r(e.currentTarget.hash)[0];s(r(t)[0],t.substr(1)),a(e.currentTarget,t.substr(1)),c.style.display="none",c.removeAttribute("data-puretabs"),l.style.display="block",l.setAttribute("data-puretabs","")},init:function(e,a){var s=this;s.className="."+e||".puretabs",s.activeClassName="."+a||".puretabs--active";var c=[].slice.call(r(s.className));c.forEach(function(e){t(e,s.activeClassName.substr(1))?r(e.hash)[0].setAttribute("data-puretabs",""):r(e.hash)[0].style.display="none",e.addEventListener("click",function(e){e.preventDefault(),s.toggle.call(s,e)})})}}}(window);

pureTabs.init('tabs__link', 'tabs__link--active');
$('.results-item__price-comfort').hide();

$('#comfort').click(function(){
  $('.results-item__price-comfort').fadeIn('5000');
  $('.results').css({"background-color": "#fff","transition": "1s ease-out", "margin-bottom":, "10px"});
});

